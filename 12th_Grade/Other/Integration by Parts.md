
## https://www.mathsisfun.com/calculus/integration-by-parts.html

# Integration by Parts

Integration by Parts is a special method of integration that is often useful when two functions are multiplied together, but is also helpful in other ways.

You will see plenty of examples soon, but first let us see the rule:

$$\int {u}{v} \, dx = {u} {\int v \, dx} - \int {u'} \left({\int v \, dx}\right) dx$$

- $u$ is the function $u(x)$
- $v$ is the function $v(x)$

## Example: What is $\int x \cos(x) \, dx$ ?

Ok, we have $x$ *multiplied by* $\cos(x)$, so integration by parts is a good choice.

First choose which functions for $u$ and $v$:

- ${u = x}$
- ${v = \cos(x)}$

### So now it is in the format $\int uv \, dx$

Differentiate $u$: ${u' = x' = 1}$

Integrate $v$: ${\int v \, dx = \int \cos(x) \, dx = \sin(x)}$

Now put everything together:

$$\begin{aligned}
\int {x} {\cos(x)} \, dx &= {x} {\sin(x)} - \int {1} \left({\sin(x)}\right) \, dx \\
&= x \sin(x) - \int \sin(x) \, dx \\
&= x \sin(x) + \cos(x) + C
\end{aligned}$$

So we followed these steps:

- Choose $u$ and $v$

- Differentiate $u$: $u'$

- Integrate $v$: $\int v \, dx$

- Put $u$, $u'$, and $\int v \, dx$ into: $u \int v \, dx - \int u' \left(\int v \, dx\right) \, dx$

- Simplify and solve

In English, to help you remember, $\int uv \, dx$ becomes:

$$\text{(u integral v) minus integral of (derivative u, integral v)}$$

## Example: What is $\int \ln(x) / x^2 \, dx$ ?

$$\begin{aligned}
u &= \ln(x) \\
v &= x^{-2} \\
\int \ln(x) / x^2 \, dx &= \ln(x) \int x^{-2} \, dx - \int \ln(x) \, \frac{d}{dx} \, \left(\int x^{-2} \, dx\right) \, dx \\
&= \ln(x) \left(-\frac{1}{x}\right) - \int \frac{1}{x} \left(-\frac{1}{x}\right) \, dx \\
&= -\frac{\ln(x)}{x} - \int -x^{-2} \, dx \\
&= -\frac{\ln(x)}{x} - x^{-1} + C \\
&= \frac{-(\ln(x) + 1)}{x} + C
\end{aligned}$$

## Example: What is $\int \ln(x) \, dx$ ?

$$\begin{aligned}
u &= \ln(x) \\
v &= 1 \\
\int \ln x \cdot 1 \, dx &= \ln x \cdot x - \int \frac{1}{x} (x) \, dx \\
&= x \ln x - \int 1 \, dx \\
&= x \ln x - x + C
\end{aligned}$$

## Example: What is $\int e^x x \, dx$ ?

$$\begin{aligned}
u &= e^x \\
v &= x \\
\int e^x x \, dx &= e^x \int x \, dx - \int e^x \, \frac{d}{dx} \, \left(\int x \,dx\right) \, dx \\
&= e^x \frac{x^2}{2} - e^x \frac{x^2}{2} + C \\\\
\hline
\text{Try again.} \\
\hline \\
u &= x \\
v &= e^x \\
\int e^x x \, dx
&= x \int e^x \, dx - \int x \, \frac{d}{dx} \, \left(\int e^x \, dx\right) \, dx \\
&= x e^x - \int 1 (e^x) \, dx \\
&= x e^x - e^x + C \\
&= e^x (x - 1) + C
\end{aligned}$$

A helpful rule of thumb is `I LATE`. Choose $u$ based on which of these comes first:

- **I**: Inverse trigonometric functions such as $\arcsin(x)$, $\arccos(x)$, $\arctan(x)$

- **L**: Logarithmic functions such as $\ln(x)$, $\log(x)$

- **A**: Algebraic functions such as $x^2$, $x^3$

- **T**: Trigonometric functions such as $\sin(x)$, $\cos(x)$, $\tan(x)$

- **E**: Exponential functions such as $e^x$, $3^x$

## Example: $\int e^x \sin(x) \, dx$

$$\begin{aligned}
u &= \sin(x) \\
v &= e^x \\
\int e^x \sin(x) \, dx &= \sin(x) \int e^x \, dx - \int e^x \, \frac{d}{dx} \left(\int \sin(x) \, dx\right) \, dx \\
&= \sin(x) \, e^x - \int \cos(x) \left(e^x\right) \, dx \\\\
\hline
\text{Continue.} \\
\hline \\
u &= \cos(x) \\
v &= e^x \\
\int \cos(x) \left(e^x\right) \, dx &= \cos(x) \int e^x \, dx - \int \cos(x) \frac{d}{dx} \left(\int e^x \, dx\right) \, dx \\
&= \cos(x) \, e^x - \int -\sin(x) \, e^x \, dx \\
\int e^x \sin(x) \, dx &= \sin(x) \, e^x - \left(\cos(x) \, e^x - \int -\sin(x) \, e^x \, dx\right) \\
&= \sin(x) \, e^x - \left(e^x \cos(x) + \int e^x \sin(x) \, dx\right) \\
&= \sin(x) \, e^x - e^x \cos(x) - \int e^x \sin(x) \, dx \\
2 \int e^x \sin(x) \, dx &= e^x \sin(x) - e^x \cos(x) \\
\int e^x \sin(x) \, dx &= \frac{e^x \left(\sin(x) - \cos(x)\right)}{2} + C
\end{aligned}$$

## Footnote: Where Did "Integration by Parts" Come From?

It is based on the Product Rule for Derivatives:

$$(uv)' = uv' + u'v$$

Integrate both sides and rearrange:

$$\begin{aligned}
\int (uv)' \, dx &= \int uv' \, dx + \int u'v \, dx \\
uv &= \int uv' \, dx + \int u'v \, dx \\
\int uv' \, dx &= uv - \int u'v \, dx
\end{aligned}$$

Some people prefer that last form, but I like to integrate $v'$ so the left side is simple:

$$\int uv \, dx = u \int v \, dx - \int u' \left(\int v \, dx\right) \, dx$$
