6.2 Partial Derivatives
=======================

A partial derivative of a function with multiple variables picks one
variable (say, $x$) and treats all other variables as fixed or constant.
We can then differentiate with respect to our chosen variable.

Notation
--------

For $z = f(x, y)$ the partial derivatives with respect to $x$ and $y$
are denoted:

$$\left(\frac{\partial z}{\partial x} \text{ or } Z_x \text{ or } f_x\right) \text{ and } \left(\frac{\partial z}{\partial y} \text{ or } Z_y \text{ or } f_y\right)$$

$w = x^2 - xy + y^2 + 2yz + 2z^2 + z$, find...

1.  $\frac{\partial w}{\partial x} = 2x - y$
2.  $\frac{\partial w}{\partial y} = -x + 2y + 2z$
3.  $\frac{\partial w}{\partial z} = 2y + 4z + 1$

Ex) For $f(x,y) = e^{xy} + y \ln x$, find $f_x$ and $f_y$.

$$\begin{aligned}
f_x &= \frac{\partial f}{\partial x} = e^{xy} \cdot y + y \cdot \frac{1}{x} \\
&= ye^{xy} + \frac{y}{x} \\
&= y \left(e^{xy} + \frac{1}{x}\right)\end{aligned}$$

Higher-Order Partial Derivatives
--------------------------------

Consider $z = f(x,y) = 3xy^2 + 2xy + x^2$

We may differentiate with respect to $x$:

$\frac{\partial z}{\partial x} = \frac{\partial f}{\partial x} = 3y^2 + 2y + 2x$

We may take a second-order partial derivative with respect to either
variable. Say $y$:

$\frac{\partial}{\partial y} \left(\frac{\partial z}{\partial x}\right)6y + 2$

### Notation

$\frac{\partial}{\partial y} = \frac{\partial^2 z}{\partial y \partial x} = f_{xy}$

### Examples

Ex) Find the four second-order partial derivatives for

$z = f(x, y) = x^2 y^3 + x^4 y + xe^y$

$$\begin{aligned}
f_{xx} &= \frac{\partial^2 f}{\partial x^2} = \frac{\partial}{\partial x}\left(2xy^3 + 4x^3y + e^y\right) \\
&= 2y^2 + 12x^2y \\
f_{xy} &= \frac{\partial^2 f}{\partial y \partial x} = \frac{\partial}{\partial y}\left(2xy^3 + 4x^3y + e^y\right) \\
&= 6xy^2 + 4x^3 + e^y \\
f_{yx} &= \frac{\partial^2 f}{\partial x \partial y} = \frac{\partial}{\partial x} \left(3x^2y^2 + x^4 + xe^y\right) \\
&= 6xy^2 + 4x^3 + e^y \\
f_{yy} &= \frac{\partial^2 f}{\partial y^2} = \frac{\partial}{\partial y} \left(3x^2y^2 + x^4 + xe^y\right) \\
&= 6x^2y + xe^y
\end{aligned}$$

#### Important note:

In this problem, $f_{xy} = f_{yx}$ or
$\frac{\partial^2 f}{\partial y \partial x} = \frac{\partial^2 f}{\partial x \partial y}$.
While this is *often* true, it is not true *in general* and cannot be
assumed.
